## Material ripple on pre lollipop devices

https://github.com/balysv/material-ripple

## Arc and reveal effect on prelollipop devices.

https://github.com/asyl/ArcAnimator

## Numlock on at startup ubuntu

sudo apt-get update
sudo apt-get -y install numlockx
sudo sed -i 's|^exit 0.*$|# Numlock enable\n[ -x /usr/bin/numlockx ] \&\& numlockx on\n\nexit 0|' /etc/rc.local

## remove non empty folder ubuntu

sudo rm -rf folderName

## JDK for ubuntu

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer

(apt-get install oracle-java8-installer)

## remove blinking o item update recyclerview

((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

## ftp not connecting google cloud fix

https://askubuntu.com/questions/420652/how-to-setup-a-restricted-sftp-server-on-ubuntu

## Configure FTP on ubuntu 16.04

https://websiteforstudents.com/setup-vsftpd-on-ubuntu-16-04-lts-server-with-ssl-tls-certificates/

after doing above add this line to the bottom

write_enable=YES

## Change user directory

sudo usermod -d /var/www/html ftpuser

## fix laravel was not found on this server

sudo a2enmod rewrite

cd /etc/apache2/mods-enabled/
sudo nano /etc/apache2/apache2.conf

Find the following code inside the editor:

<Directory /var/www/> 
   Options Indexes FollowSymLinks
   AllowOverride None
   Require all granted
</Directory> 
Change to:

<Directory /var/www/> 
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>

sudo service apache2 restart

##Public permission AWS S3

{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicRead",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::mylifelines-userfiles-mobilehub-1936027340/*"
        }
    ]
}